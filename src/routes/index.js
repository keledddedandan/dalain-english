import Vue from 'vue'

import VueRouter from 'vue-router'
Vue.use(VueRouter);
import less from '../components/less'
import active from '../components/active'
import news from '../components/news'

var routerOne = new VueRouter({
    routes: [
        {
            path:'/less',
            name:'less',
            component:less
            //每一个链接都是一个对象
            //链接路径
            //路由名称，
            //对应的组件模板
            //配置路由，这里是个数组
        },
        {
            path: '/active',
            name: 'active',
            component: active
        },
        {
            path: '/news',
            name: 'news',
            component: news
        },

    ]
});
export default routerOne