import Vue from 'vue'
import App from './App.vue'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
Vue.use(ElementUI)
import Axios from 'axios'
Axios.defaults.withCredentials=true;//让ajax携带cookie
Vue.prototype.$axios = Axios
Axios.defaults.baseURL='http://47.92.50.43:8888/'
import VueRouter from 'vue-router'
Vue.use(VueRouter)
import router from '@/routes'
import VueSwiper from 'vue-awesome-swiper'
Vue.use(VueSwiper)
import 'swiper/dist/css/swiper.css'

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
  router
}).$mount('#app')
